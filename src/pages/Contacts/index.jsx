import React, { useContext } from "react";
import List from "../../components/List";
import UsersContext from "../../store";


const demoContact = {
  1: {
    id: 1,
    name: "john",
  },
  2: {
    id: 2,
    name: "McG",
  },
  10: {
    id: 10,
    name: "Mr J",
  },
  20: {
    id: 20,
    name: "Mrs K",
  },
  30: {
    id: 30,
    name: "Roland",
  },
};

const ContactList = () => {
  const { updateUserStatus } = useContext(UsersContext);

  const addUserToList = (i) => {
    updateUserStatus(demoContact[i]);
  };

  return (
    <div>
      <h3 style={{color: "white", padding: "15px"}}>Contact List</h3>
      <List usersList={demoContact} onClick={addUserToList} />
    </div>
  );
};

export default ContactList;
