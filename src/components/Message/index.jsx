import React from "react";
import css from "./message.module.css";

const Message = (props) => {
  const { sent, children, time } = props;
  return (
    <div className={css.msgWrap}>
      <span className={`${css.msgTxt} ${sent ? css.sent : css.recieved}`}>
        <pre>{children}</pre>
        <small className={css.time}>{time}</small>
      </span>
    </div>
  );
};

export default Message;
