import React, { useContext } from "react";
import css from "./user.module.css";
import { Avatar, Row, Col } from "antd";
import { PhoneFilled, UserOutlined } from "@ant-design/icons";
import UsersContext from "../../store";

const User = (props) => {
  const { onClose, userId } = props;
  const { users } = useContext(UsersContext);

  const closeBtn = () => {
    onClose();
  };

  return (
    <div style={{ width: "100%" }}>
      <div className={css.header}>
        <Row style={{ padding: "20px" }}>
          <Col span={12} style={{ textAlign: "left" }}>
            <strong>Contact Info</strong>
          </Col>
          <Col span={12} style={{ textAlign: "right" }}>
            <a href="#" onClick={closeBtn}>
              <strong>Close</strong>
            </a>
          </Col>
        </Row>
        <Row align="middle" className={css.info}>
          <Col span={12}>
            <Avatar className={css.avatar} size={100} icon={<UserOutlined />} />
          </Col>
          <Col span={12}>
            <div className={css.infoTxt}>
              <strong>{users[userId].name} </strong>
              <small>Last seen {users[userId].lastMessage ? users[userId].lastMessage.time : '-'}</small>
            </div>
          </Col>
        </Row>
      </div>
      <div className={css.body}>
        <Row>
          <Col span={12} className={css.title}>
            <PhoneFilled rotate={90} />
          </Col>
          <Col span={12}>093487287362365</Col>
        </Row>
        <Row>
          <Col span={12} className={css.title}>
            User ID
          </Col>
          <Col span={12}>{userId}</Col>
        </Row>
      </div>
    </div>
  );
};

export default User;
