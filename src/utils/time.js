export function timeStamp(initTime) {
  if (initTime) {
    return initTime.getHours() + ":" + initTime.getMinutes();
  }
  const time = new Date();
  return time.getHours() + ":" + time.getMinutes();
}
